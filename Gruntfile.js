require('dotenv').config();

module.exports = function(grunt) {

  grunt.initConfig({
  	pkg: grunt.file.readJSON('package.json'),
	cacheBuster: Math.random().toString(36).substring(7),
	cwd: '<%= `${grunt.config("pkg").evomob.projectHash}/${grunt.config("pkg").evomob.formFactor}` %>',
	jsAssets: [],
	cssAssets: [],
	exec: {
		remix: {
			cmd: 'remix build'
		}
	},
	clean: ['bld/javascripts', 'bld/stylesheets'],
  	concat: {
  		/*
  		 * CSS concatenation
  		 */
  		css: {
  			options: {
				stripBanners: true,
				separator: '\n',
	  			banner: '\
/*\n\
 * Project name: <%= pkg.name %>\n\
 * Form factor:  <%= pkg.evomob.formFactor %>\n\
 * Version:      <%= pkg.version %>\n\
 */\n'
  			},
	  		src: [ '<%= cssAssets %>' ],
	  		dest: 'bld/application-<%= cacheBuster %>.min.css'
		},
		/*
		 * JS Concatenation
		 */
		js: {
			options: {
				stripBanners: true,
				separator: ';\n',
	  			banner: '\
/*\n\
 * Project name: <%= pkg.name %>\n\
 * Form factor:  <%= pkg.evomob.formFactor %>\n\
 * Version:      <%= pkg.version %>\n\
 */\n'
  			},
			src: [ '<%= jsAssets %>' ],
			dest: 'bld/application-<%= cacheBuster %>.min.js'
		},
		/*
		 * Remix (Just adds the banner to the Remix file)
		 */
		remix: {
			options: {
	  			banner: '\
/*\n\
 * Project name: <%= pkg.name %>\n\
 * Form factor:  <%= pkg.evomob.formFactor %>\n\
 * Version:      <%= pkg.version %>\n\
 * Environment:  <%= grunt.option("env") ? grunt.option("env") : "Production" %>\n\
 */\n'
  			},
			src: [ 'bld/remix.js' ],
			dest: 'bld/remix.js'
		}
	},
	compress: {
	  gzip: {
		options: {
		  mode: 'gzip'
		},
		expand: true,
		cwd: 'bld/',
		src: ['*'],
		dest: 'bld/'
	  }
	},
	release: {
		options: {
            beforeRelease: [ { name: 'build', preserveFlags: true } ],
			npm: false,
		}
	},
	replace: {
		paths: {
			files: [
    	        {
    	            cwd: 'bld/',
    	            dest: 'bld/',
    	            expand: true,
    	            src: [ 'remix.js', 'application-*' ]
    	        }
    	    ],
			options : {
				patterns: [
					// Replace images
					{
						match: /\/\/localhost:[8043]{4}\/images|\.\.\/images/g,
						replacement: function() {
							var env = grunt.option('env'),
							    domain = env === 'staging' ? 'test.evomob.com' : 'images.evomob.com';
							
							return `//${domain}/${grunt.config('cwd')}` + (env === 'staging' ? '/images' : '');
						}
					},
					// Replace any other path
					{
						match: /\/\/localhost:[8043]{4}/g,
						replacement: function() {
							var domain = grunt.option('env') === 'staging' ? 'test.evomob.com' : 'live.evomob.com';
							
							return `//${domain}/${grunt.config('cwd')}`;
						}
					}
				],
                preserveOrder: true
			}
		},
    	bundle: {
    	    files: [
    	        {
    	            cwd: 'bld/',
    	            dest: 'bld/',
    	            expand: true,
    	            src: [ 'remix.js' ]
    	        }
    	    ],
    	    options: {
    	        patterns: [
					// JS Bundle
    	            {
    	                match: /\<\!\-\-JS_bundle_starts[\s\S]*JS_bundle_ends\-\-\>/,
    	                replacement: function (match) {
							var domain = grunt.option('env') === 'staging' ? 'test.evomob.com' : 'live.evomob.com',
    	                    //Grab all of the filenames ending in .js
							    javascriptAssets = match.match(/[^/\\]+(?:\.js)/gi).map(function(filename){
									return `bld/javascripts/${filename}`;
								});
							
							grunt.config('jsAssets', javascriptAssets);
    	                	
    	                    //Replace the block with this <script> tag
    	                    return `<script type="text/javascript" src="//${domain}/${grunt.config('cwd')}/application-${grunt.config('cacheBuster')}.min.js"></script>`;
    	                }
    	            },
					// CSS Bundle
    	            {
    	                match: /\<\!\-\-CSS_bundle_starts[\s\S]*CSS_bundle_ends\-\-\>/,
    	                replacement: function (match) {
							var domain = grunt.option('env') === 'staging' ? 'test.evomob.com' : 'live.evomob.com',
    	                    	//Grab all of the filenames ending in .js
							    cssAssets = match.match(/[^/\\]+(?:\.css)/gi).map(function(filename){
									return `bld/stylesheets/${filename}`;
						        });
							
							grunt.config('cssAssets', cssAssets);
    	                	
    	                    //Replace the block with this <link> tag
    	                    return `<link rel="stylesheet" href="//${domain}/${grunt.config('cwd')}/application-${grunt.config('cacheBuster')}.min.css"/>`;
    	                }
    	            }
    	        ]
    	    }
    	}
    },
	tag: {
		options: {
			tagMsg: 'Version <%= version %> deployed in <%= grunt.option("env") ? grunt.option("env") : "production" %>',
			tagName: 'deployed-in-<%= grunt.option("env") ? grunt.option("env") : "production" %>'
		}
	},
	aws_s3: {
		options: {
		  accessKeyId: process.env.AWS_ACCESS_KEY,
		  secretAccessKey: process.env.AWS_SECRET,
		  region: 'eu-west-1',
		  bucket: '<%= grunt.option("env") === "staging" ? "test.evomob.com" : "live.evomob.com" %>',
		  access: 'public-read',
		  params: {
			// One Year cache policy (1000 * 60 * 60 * 24 * 365)
		  	CacheControl: "max-age=31536000, public, must-revalidate",
			Expires: new Date(Date.now() + 31536000000).toISOString()
		  }
		},
		clean_assets: {
			files: [
				{ 
					dest: '<%= `${grunt.config("cwd")}/` %>', 
					action: 'delete', 
					exclude: '*'
				}
			]
		},
		clean_production_images: {
			options: { bucket: 'images.evomob.com' },
			files: [
				{ 
					dest: '<%= `${grunt.config("cwd")}/` %>',
					action: 'delete', 
					exclude: '*'
				}
			]
		},
		deploy_assets: {
			options: {
				params: {
					ContentEncoding: 'gzip'
				}
			},
			files: [
				{ 
					cwd: 'bld/',
					src: ['*'],
					dest: '<%= `${grunt.config("cwd")}/` %>', 
					expand: true
				},
			]
		},
		deploy_production_images: {
			options: { bucket: 'images.evomob.com' },
			files: [
				{ 
					cwd: 'bld/images',
					src: ['*'],
					dest: '<%= `${grunt.config("cwd")}/` %>', 
					expand: true
				},
			]
		},
		deploy_staging_images: {
			options: { bucket: 'test.evomob.com' },
			files: [
				{ 
					cwd: 'bld/',
					src: ['images/*'],
					dest: '<%= `${grunt.config("cwd")}/` %>', 
					expand: true
				},
			]
		}
	}
  });

  // Load plugins here
  grunt.loadNpmTasks('grunt-aws-s3');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-compress');
  grunt.loadNpmTasks('grunt-exec');
  grunt.loadNpmTasks('grunt-release');
  grunt.loadNpmTasks('grunt-replace');
  grunt.loadNpmTasks('grunt-tag');

  // Define your tasks here
  grunt.registerTask('build', [
	'exec',
  	'replace:bundle',
	'concat',
	'replace:paths',
	'clean',
	'compress'
  ]);
	
  grunt.registerTask('deploy', 'Cleans and deploys the assets on the appropriate S3 bucket, and tags the current commit as deployed in the specified environment. Use `--env=staging` to deploy in staging (test.evomob.com), leave blank to deploy in production (live.evomob.com and images.evomob.com).', function(){
	  var env = grunt.option('env');
	  
	  grunt.task.run(['aws_s3:clean_assets', 'aws_s3:deploy_assets']);
	  
	  if(env === 'staging') {
	  	grunt.task.run('aws_s3:deploy_staging_images');
	  }
	  else {
		grunt.task.run(['aws_s3:clean_production_images', 'aws_s3:deploy_production_images']);
	  }
	  
	  grunt.task.run('tag');
  });
	
  grunt.registerTask('publish', 'Stages all changed files in git, bumps the version according to the `--type` flag, commits on the current branch and deploys to the correct environment set by the `--env` flag. @See `grunt deploy`.', function(){
	  var type = grunt.option('type');
	  
	  grunt.task.run(`release:${type}`);
	  grunt.task.run('deploy');
  });	

};